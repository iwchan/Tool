﻿using Check.Utility;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Check.Views
{
    /// <summary>
    /// Interaction logic for HashView.xaml
    /// </summary>
    public partial class HashView : UserControl
    {
        public HashView()
        {
            InitializeComponent();
        }

        private async void Button_Click_To_Hash(object sender, RoutedEventArgs e)
        {
            tb_rst.Clear();
            var context = tb_raw.Text.Trim();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (string.IsNullOrEmpty(context))
            {
                await metroWindow.ShowMessageAsync("错误", "输入为空，请输入要转换的文本！");
                return;
            }

            var rst = "";
            switch (cb_type.Text)
            {
                case "MD5":
                    rst = HashUtil.ToMD5(context);
                    break;
                case "SHA-1":
                    rst = HashUtil.ToSHA1(context);
                    break;
                case "SHA-256":
                    rst = HashUtil.ToSHA256(context);
                    break;
                case "SHA-384":
                    rst = HashUtil.ToSHA384(context);
                    break;
                case "SHA-512":
                    rst = HashUtil.ToSHA512(context);
                    break;
            }
            tb_rst.Text = rst;
        }

        private async void Button_Click_To_Hmac_Hash(object sender, RoutedEventArgs e)
        {
            tb_rst.Clear();
            var context = tb_raw.Text.Trim();
            var pwd = tb_pwd.Text.Trim();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (string.IsNullOrEmpty(context))
            {
                await metroWindow.ShowMessageAsync("错误", "输入内容为空，请输入要转换的文本！");
                return;
            }
            if (string.IsNullOrEmpty(pwd))
            {
                await metroWindow.ShowMessageAsync("错误", "输入密码为空，请输入必要的密码！");
                return;
            }


            var rst = "";
            switch (cb_hmac_type.Text)
            {
                case "HmacMD5":
                    rst = HashUtil.ToHmacMD5(context, pwd);
                    break;
                case "HmacSHA-1":
                    rst = HashUtil.ToHmacSHA1(context, pwd);
                    break;
                case "HmacSHA-256":
                    rst = HashUtil.ToHmacSHA256(context, pwd);
                    break;
                case "HmacSHA-384":
                    rst = HashUtil.ToHmacSHA384(context, pwd);
                    break;
                case "HmacSHA-512":
                    rst = HashUtil.ToHmacSHA512(context, pwd);
                    break;
            }
            tb_rst.Text = rst;
        }

        private async void Button_Click_To_UrlEncode(object sender, RoutedEventArgs e)
        {
            tb_rst.Clear();
            var context = tb_raw.Text.Trim();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (string.IsNullOrEmpty(context))
            {
                await metroWindow.ShowMessageAsync("错误", "输入内容为空，请输入要转换的文本！");
                return;
            }
            var handledText = "";
            if ((bool)rb_type.IsChecked)
            {
                handledText = HttpUtility.UrlEncode(context);
            }
            else
            {
                handledText = HttpUtility.UrlDecode(context);
            }

            tb_url_encode_out.Text = handledText;
        }

        private async void Button_Click_To_Base64Encode(object sender, RoutedEventArgs e)
        {
            tb_rst.Clear();
            var context = tb_raw.Text.Trim();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (string.IsNullOrEmpty(context))
            {
                await metroWindow.ShowMessageAsync("错误", "输入内容为空，请输入要转换的文本！");
                return;
            }
            var encode = Encoding.GetEncoding(cb_base64_encode.Text.Trim());
            var handledText = "";

            try
            {
                if ((bool)rb_base64_type.IsChecked)
                {
                    handledText = EncodeUtil.EncodeBase64(encode, context);
                }
                else
                {
                    handledText = EncodeUtil.DecodeBase64(encode, context);
                }
            }
            catch (Exception ex)
            {
                await metroWindow.ShowMessageAsync("异常", ex.Message);
            }
            tb_base64_encode_out.Text = handledText;
        }

        private async void Button_Click_To_Unicode(object sender, RoutedEventArgs e)
        {
            tb_rst.Clear();
            var context = tb_raw.Text.Trim();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (string.IsNullOrEmpty(context))
            {
                await metroWindow.ShowMessageAsync("错误", "输入内容为空，请输入要转换的文本！");
                return;
            }
            var prefix = cb_unicode_prefix.Text.Trim();

            var handledText = "";
            if ((bool)rb_unicode_type.IsChecked)
            {
                switch (prefix)
                {
                    case "Unicode":
                        handledText = EncodeUtil.StringToUnicode(context);
                        break;
                }
            }
            else
            {
                try
                {
                    switch (prefix)
                    {
                        case "Unicode":
                            handledText = EncodeUtil.UnicodeToString(context);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    await metroWindow.ShowMessageAsync("异常", ex.Message);
                }
            }

            tb_unicode_encode_out.Text = handledText;
        }
    }
}
