﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Check.Utility
{
    class EncodeUtil
    {
        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="encoding">加密采用的编码方式</param>
        /// <param name="content">待加密的明文</param>
        /// <returns></returns>
        public static string EncodeBase64(Encoding encoding, string content)
        {
            var encode = "";
            var bytes = encoding.GetBytes(content);
            encode = Convert.ToBase64String(bytes);
            return encode;
        }


        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="encoding">解密采用的编码方式，注意和加密时采用的方式一致</param>
        /// <param name="content">待解密的密文</param>
        /// <returns>解密后的字符串</returns>
        public static string DecodeBase64(Encoding encoding, string content)
        {
            var decode = "";
            var bytes = Convert.FromBase64String(content);
            decode = encoding.GetString(bytes);
            return decode;
        }

        /// <summary>  
        /// 字符串转Unicode;\u前缀  
        /// </summary>  
        /// <param name="content">源字符串</param>  
        /// <returns>Unicode编码后的字符串</returns>  
        public static string StringToUnicode(string content)
        {
            var bytes = Encoding.Unicode.GetBytes(content);
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < bytes.Length; i += 2)
            {
                stringBuilder.AppendFormat($"\\u{bytes[i + 1]:x2}{bytes[i]:x2}");
            }
            return stringBuilder.ToString();
        }

        /// <summary>    
        /// 把&#前缀的Unicode字符串转为正常字符串    
        /// </summary>    
        /// <param name="content"></param>    
        /// <returns></returns>    
        public static string UnicodeToString(string content)
        {
            var dst = "";
            var src = content;
            var len = content.Length;
            for (var i = 0; i < len; i++)
            {
                if (src.StartsWith("\\u") && src.Length >= 6)
                {
                    var str = "";
                    str = src.Substring(0, 6).Substring(2);

                    var bytes = new byte[2];
                    bytes[1] = byte.Parse(int.Parse(str.Substring(0, 2), System.Globalization.NumberStyles.HexNumber).ToString());
                    bytes[0] = byte.Parse(int.Parse(str.Substring(2, 2), System.Globalization.NumberStyles.HexNumber).ToString());
                    dst += Encoding.Unicode.GetString(bytes);

                    src = src.Substring(6);
                    i += 5;
                }
                else
                {
                    dst += src[0];
                    src = src.Substring(1);
                }
            }
            return dst;
        }
    }
}
