﻿using Check.Entity;
using HtmlAgilityPack;
using Http;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Supremes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Check.Views
{
    /// <summary>
    /// Interaction logic for XpathView.xaml
    /// </summary>
    public partial class SelectorView : UserControl
    {
        public SelectorView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button_Click_Check(object sender, RoutedEventArgs e)
        {
            tv_rst.Items.Clear();
            var url = tb_url.Text.Trim();
            var rule = tb_xpath.Text.Trim();
            var encode = cb_encode.Text.Trim();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (string.IsNullOrEmpty(url))
            {
                await metroWindow.ShowMessageAsync("错误", "链接为空，请输入链接！");
                return;
            }

            if (!CheckUrl(url))
            {
                await metroWindow.ShowMessageAsync("错误", "链接格式错误，请检查链接格式！");
                return;
            }
            if (string.IsNullOrEmpty(rule))
            {
                await metroWindow.ShowMessageAsync("错误", "xpath/css为空，请输入xpath/css！");
                return;
            }

            var rsp = Request.Get(url, encode);
            if (rsp.OK)
            {
                try
                {
                    var nodes = new List<Node>();
                    if ((bool)rb_type.IsChecked)
                    {
                        nodes = InXpath(rule, rsp.Content);
                    }
                    else
                    {
                        nodes = InCSS(rule, rsp.Content);
                    }
                    CreateTreeNodes(nodes);
                    tb_tips.Text = $"共有{nodes.Count}个结果";
                    fo_tips.IsOpen = !fo_tips.IsOpen;
                }
                catch (Exception ex)
                {
                    await metroWindow.ShowMessageAsync("解析HTML文档异常", $"{ex.Message}");
                }
            }
            else
            {
                var message = rsp.Exception.Message;
                await metroWindow.ShowMessageAsync("请求链接异常", $"{message}");
            }
        }

        /// <summary>
        /// 使用CSS选择器
        /// </summary>
        /// <param name="css"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        private static List<Node> InCSS(string css, string content)
        {
            var nodes = new List<Node>();
            var doc = Dcsoup.Parse(content);
            var elements = doc.Select(css);
            var Count = 0;
            if (elements != null)
            {
                foreach (var element in elements)
                {
                    var node = new Node
                    {
                        Name = $"Node[{Count}]",
                        Content = element.Text,
                        InnerHtml = element.Html,
                        OuterHtml = element.OuterHtml
                    };
                    nodes.Add(node);
                    Count++;
                }
            }
            return nodes;
        }

        /// <summary>
        /// 使用xpath选择器解析文档
        /// </summary>
        /// <param name="xpath"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        private static List<Node> InXpath(string xpath, string content)
        {
            var nodes = new List<Node>();
            var hd = new HtmlDocument();
            hd.LoadHtml(content);

            var hnc = hd.DocumentNode.SelectNodes(xpath);
            var Count = 0;
            if (hnc != null)
            {
                foreach (var hn in hnc)
                {
                    var node = new Node
                    {
                        Name = $"Node[{Count}]",
                        Content = hn.InnerText,
                        InnerHtml = hn.InnerHtml,
                        OuterHtml = hn.OuterHtml
                    };
                    nodes.Add(node);
                    Count++;
                }
            }
            return nodes;
        }

        /// <summary>
        /// 生成节点
        /// </summary>
        /// <param name="nodes"></param>
        private void CreateTreeNodes(List<Node> nodes)
        {
            foreach (var node in nodes)
            {
                if (!string.IsNullOrEmpty(node.Content))
                {
                    var bgBrush = new SolidColorBrush(Color.FromRgb(33, 36, 42));
                    var subBgBrush = new SolidColorBrush(Color.FromRgb(40, 44, 52));
                    var fgBrush = new SolidColorBrush(Color.FromRgb(171, 178, 191));
                    var item = new TreeViewItem
                    {
                        Header = node.Name,
                        Background = bgBrush,
                        Foreground = fgBrush
                    };
                    var subItem = new TreeViewItem
                    {
                        Header = node.Content.Trim(),
                        Background = subBgBrush,
                        Foreground = fgBrush
                    };
                    if (!string.IsNullOrEmpty(node.OuterHtml))
                    {
                        var outerItem = new TreeViewItem
                        {
                            Header = "OuterHtml",
                            Background = subBgBrush,
                            Foreground = fgBrush
                        };
                        var outerHtmlItem = new TreeViewItem
                        {
                            Header = node.OuterHtml,
                            Background = subBgBrush,
                            Foreground = fgBrush
                        };
                        outerItem.Items.Add(outerHtmlItem);
                        subItem.Items.Add(outerItem);
                    }

                    if (!string.IsNullOrEmpty(node.InnerHtml))
                    {
                        var innerItem = new TreeViewItem
                        {
                            Header = "InnerHtml",
                            Background = subBgBrush,
                            Foreground = fgBrush
                        };
                        var innerHtmlItem = new TreeViewItem
                        {
                            Header = node.InnerHtml,
                            Background = subBgBrush,
                            Foreground = fgBrush
                        };
                        innerItem.Items.Add(innerHtmlItem);
                        subItem.Items.Add(innerItem);
                    }

                    item.ExpandSubtree();
                    item.Items.Add(subItem);

                    tv_rst.Items.Add(item);
                }
            }
        }

        private static bool CheckUrl(string url)
        {
            var isRight = false;
            if (url.StartsWith("http://") || url.StartsWith("https://"))
            {
                isRight = true;
            }
            return isRight;
        }
    }
}
