﻿using Check.Utility;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Check.Views
{
    /// <summary>
    /// Interaction logic for RegexView.xaml
    /// </summary>
    public partial class RegexView : UserControl
    {
        public RegexView()
        {
            InitializeComponent();
        }

        private async void Button_Click_Check(object sender, RoutedEventArgs e)
        {
            tv_rst.Items.Clear();
            var tr = new TextRange(tb_raw.Document.ContentStart, tb_raw.Document.ContentEnd);
            var raw = tr.Text.Trim();
            var pattern = tb_rule.Text.Trim();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);

            if (string.IsNullOrEmpty(pattern))
            {
                await metroWindow.ShowMessageAsync("错误", "正则表达式为空，请输入正则表达式！");
                return;
            }
            if (string.IsNullOrEmpty(raw))
            {
                await metroWindow.ShowMessageAsync("错误", "文本为空，请输入要检测的文本！");
                return;
            }
            try
            {
                var regexOptions = GetRegexOptions();
                var regex = new Regex(pattern, regexOptions);
                var matchCollection = regex.Matches(raw);
                var matchesFound = matchCollection.Count;

                if ((bool)cb_replace_mode.IsChecked)
                {
                    tb_raw.Document.Blocks.Clear();
                    tb_raw.AppendText(regex.Replace(raw, pattern));
                }

                var groupNumbers = regex.GetGroupNumbers();
                var groupNames = regex.GetGroupNames();

                var bgBrush = new SolidColorBrush(Color.FromRgb(33, 36, 42));
                var subBgBrush = new SolidColorBrush(Color.FromRgb(40, 44, 52));
                var fgBrush = new SolidColorBrush(Color.FromRgb(171, 178, 191));
                foreach (var groupNumber in groupNumbers)
                {
                    if (groupNumber > 0)
                    {
                        var groupName = $"Group[{groupNumber}] ";
                        if (groupNames[groupNumber] != groupNumber.ToString())
                        {
                            groupName += " (" + groupNames[groupNumber] + ")";
                        }
                    }
                }

                var count = 0;
                foreach (Match match in matchCollection)
                {
                    var item = new TreeViewItem
                    {
                        Header = $"Matches[{count}]",
                        Background = bgBrush,
                        Foreground = fgBrush
                    };


                    for (var i = 0; i < match.Groups.Count; i++)
                    {
                        var group = match.Groups[i];
                        if (group.Captures.Count == 1)
                        {
                            var val = group.Value;
                            var name = $"Groups[{groupNames[i]}]";
                            if (StrUtil.IsNumeric(groupNames[i]))
                            {
                                name = $"Groups[{groupNames[i]}]";
                            }
                            else
                            {
                                name = $"Groups[\"{groupNames[i]}\"]";
                            }

                            var subItems = new TreeViewItem
                            {
                                Header = name,
                                Background = subBgBrush,
                                Foreground = fgBrush
                            };
                            var subSubSubItems = new TreeViewItem
                            {
                                Header = val,
                                Background = subBgBrush,
                                Foreground = fgBrush
                            };
                            subItems.Items.Add(subSubSubItems);
                            item.Items.Add(subItems);
                        }
                    }
                    item.ExpandSubtree();
                    if (item.Items.Count > 0)
                    {
                        tv_rst.Items.Add(item);
                    }
                    count++;
                }

                var flyout = new Flyout
                {
                    Background = fgBrush,
                    Name = "tips",
                    IsPinned = false,
                    Position = Position.Bottom,
                    TitleVisibility = Visibility.Collapsed,
                    IsAutoCloseEnabled = true,

                };
                tb_tips.Text = $"共匹配到{count}个结果";
                fo_tips.IsOpen = !fo_tips.IsOpen;
            }
            catch (Exception ex)
            {
                await metroWindow.ShowMessageAsync("异常", ex.Message);
            }
        }

        private RegexOptions GetRegexOptions()
        {
            var regexOptions = new RegexOptions();
            if ((bool)cb_ignore_case.IsChecked)
            {
                regexOptions |= RegexOptions.IgnoreCase;
            }
            if ((bool)cb_multiline.IsChecked)
            {
                regexOptions |= RegexOptions.Multiline;
            }

            if ((bool)cb_singleline.IsChecked)
            {
                regexOptions |= RegexOptions.Singleline;
            }

            if ((bool)cb_indented_input.IsChecked)
            {
                regexOptions |= RegexOptions.IgnorePatternWhitespace;
            }

            return regexOptions;
        }

        private void Button_Click_Setting(object sender, RoutedEventArgs e)
        {
            settingsFlyout.IsOpen = !settingsFlyout.IsOpen;
        }
    }
}
