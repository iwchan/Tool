﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Http
{
    public class Header
    {
        public string Method { get; set; } = "Get";

        public string Accept { get; set; } = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";

        public string UserAgent { get; set; } = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";

        public string AcceptEncoding { get; set; } = "gzip,deflate,sdch";

        public string AcceptLanguag { get; set; } = "zh-CN,zh;q=0.8";

        public bool KeepAlive { get; set; } = true;

        public CookieContainer Cookies { get; set; } = null;

        public DecompressionMethods AutomaticDecompression { get; set; } = DecompressionMethods.Deflate | DecompressionMethods.GZip;

        public string Referer { get; set; } = "";


        public static void AddHeader(HttpWebRequest request, Header header = null)
        {
            if (header is null)
            {
                header = new Header();
            }

            request.Accept = header.Accept;
            request.UserAgent = header.UserAgent;
            request.KeepAlive = header.KeepAlive;
            request.Headers.Add("Accept-Encoding", header.AcceptEncoding);
            request.Headers.Add("Accept-Language", header.AcceptLanguag);
            request.AutomaticDecompression = header.AutomaticDecompression;
            request.Method = header.Method;
            request.Referer = header.Referer;
            if (header.Cookies != null)
            {
                request.CookieContainer = header.Cookies;
            }
        }

        public static void AddHeader(HttpWebRequest request, int dataLength, Header header = null)
        {
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = dataLength;
        }
    }
}
