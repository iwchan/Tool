﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Check.Entity
{
    class Index
    {
        public string Name { get; set; }

        public int Count { get; set; }

        public double Rate { get; set; }
    }
}
