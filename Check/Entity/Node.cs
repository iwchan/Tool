﻿namespace Check.Views
{
    internal class Node
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string OuterHtml { get; set; }
        public string InnerHtml { get; set; }
    }
}