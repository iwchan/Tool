﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Http
{
    public class Response
    {
        public string Content { get; set; } = "";

        public string Url { get; set; } = "";

        public bool OK { get; set; } = false;

        public int StatusCode { get; set; }

        public CookieContainer Cookie { get; set; } = null;

        public Exception Exception { get; set; } = null;
    }
}
