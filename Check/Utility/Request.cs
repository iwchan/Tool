﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Http
{
    public class Request
    {
        public static Response Get(string url, String encodingStr = null, Header header = null)
        {
            var encoding = GetEncoding(encodingStr);
            var response = new Response();

            try
            {
                var request = WebRequest.Create(url) as HttpWebRequest;
                Header.AddHeader(request, header);

                var webResponse = request.GetResponse() as HttpWebResponse;
                var stream = webResponse.GetResponseStream();
                var streamReader = new StreamReader(stream, encoding);
                var content = streamReader.ReadToEnd();
                var sc = (int)webResponse.StatusCode;
                var cookieContainer = new CookieContainer();
                cookieContainer.Add(webResponse.Cookies);

                webResponse?.Close();
                streamReader?.Close();
                stream?.Close();

                response.OK = true;
                response.Cookie = cookieContainer;
                response.Content = content;
                response.StatusCode = sc;
            }
            catch (Exception e)
            {
                response.OK = false;
                response.Exception = e;
            }
            response.Url = url;
            return response;
        }

        public static Response Post(string url, string content, Header header = null)
        {
            var response = new Response();
            var data = Encoding.UTF8.GetBytes(content);

            var req = WebRequest.Create(url) as HttpWebRequest;
            Header.AddHeader(req, data.Length, header);
            var stream = req.GetRequestStream();
            stream.Write(data, 0, data.Length);

            var webResponse = req.GetResponse() as HttpWebResponse;
            var streamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.Default);
            response.Content = streamReader.ReadToEnd();

            var cookieContainer = new CookieContainer();
            cookieContainer.Add(webResponse.Cookies);
            response.Cookie = cookieContainer;

            response.StatusCode = (int)webResponse.StatusCode;
            response.Url = url;
            response.OK = true;

            webResponse?.Close();
            streamReader?.Close();
            stream?.Close();
            return response;
        }

        private static Encoding GetEncoding(string encodingStr)
        {
            Encoding encoding = null;

            if (string.IsNullOrEmpty(encodingStr))
            {
                encoding = Encoding.UTF8;
            }
            else
            {
                encoding = Encoding.GetEncoding(encodingStr);
            }

            return encoding;
        }


    }
}
