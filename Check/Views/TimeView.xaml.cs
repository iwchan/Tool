﻿using Check.Utility;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Check.Views
{
    /// <summary>
    /// Interaction logic for Time.xaml
    /// </summary>
    public partial class TimeView : UserControl
    {
        public TimeView()
        {
            InitializeComponent();

            tb_ts_in.Clear();
            tb_today.AppendText($"{DateTime.Today:yyyy年MM月dd日}");
        }

        private async void Button_Click_To_TS(object sender, RoutedEventArgs e)
        {
            tb_ts_out.Clear();
            var sd = dtp_in.SelectedDate;
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (sd == null)
            {
                await metroWindow.ShowMessageAsync("错误", "时间为空，请选择时间！");
                return;
            }
            DateTime selectedDateTime = Convert.ToDateTime(sd);
            ChangeField(selectedDateTime);
        }

        private async void Button_Click_To_DT(object sender, RoutedEventArgs e)
        {
            string context = tb_ts_in.Text.Trim().Replace("/r", "").Replace("/n", "");
            long.TryParse(context, out long ts);

            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (string.IsNullOrEmpty(context))
            {
                await metroWindow.ShowMessageAsync("错误", "时间戳为空，请输入时间戳！");
                return;
            }

            var rule = "^[0-9]*$";
            var match = Regex.Match(context, rule);

            if (!match.Success)
            {
                await metroWindow.ShowMessageAsync("错误", "时间戳格式错误，请输入正确的时间戳！");
                return;
            }

            if (context.Length < 9)
            {
                await metroWindow.ShowMessageAsync("错误", "时间戳过短，请输入正确的时间戳！");
                return;
            }


            DateTime dt = new DateTime(0);
            if (context.Length == 9)
            {
                dt = TimeOperation.GetTime(ts * 10000);
            }
            else if (context.Length == 10)
            {
                dt = TimeOperation.GetTime(ts * 1000);
            }
            else if (context.Length == 13)
            {
                dt = TimeOperation.GetTime(ts);
            }

            date_time_picker_show.SelectedDate = dt;
        }

        private void ChangeField(DateTime now)
        {
            long ts = TimeOperation.ConvertDateTimeToInt(now);
            tb_ts_out.Clear();
            tb_ts_out.AppendText(ts.ToString());
        }

        private void Button_Click_Now_To_TS(object sender, RoutedEventArgs e)
        {
            var selectedDateTime = Convert.ToDateTime(now_time_picker.SelectedDate);
            var ts = TimeOperation.ConvertDateTimeToInt(selectedDateTime);
            tb_now_ts_out.Clear();
            tb_now_ts_out.AppendText(ts.ToString());
        }

        private void Button_Click_Clear_TS(object sender, RoutedEventArgs e)
        {
            dtp_in.ClearValue(DatePicker.SelectedDateProperty);
            tb_ts_out.Clear();
        }

        private void Button_Click_Clear_Date(object sender, RoutedEventArgs e)
        {
            date_time_picker_show.ClearValue(DatePicker.SelectedDateProperty);
            tb_ts_in.Clear();
        }

        private void Button_Click_Clear_Now(object sender, RoutedEventArgs e)
        {
            tb_now_ts_out.Clear();
        }

        private void Button_Click_Today_To_TS(object sender, RoutedEventArgs e)
        {
            tb_today_ts_out.Clear();
            tb_today.Clear();
            tb_today.AppendText($"{DateTime.Today:yyyy年MM月dd日}");
            var ts = TimeOperation.ConvertDateTimeToInt(DateTime.Today);
            tb_today_ts_out.AppendText(ts + "");
        }

        private void Button_Click_Clear_Today(object sender, RoutedEventArgs e)
        {
            tb_today_ts_out.Clear();
        }
    }
}
