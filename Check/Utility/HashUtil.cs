﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Check.Utility
{
    class HashUtil
    {
        /// <summary>
        /// 字符串 -> MD5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToMD5(string str)
        {
            var data = Encoding.UTF8.GetBytes(str);
            var md5 = new MD5CryptoServiceProvider();
            var outBytes = md5.ComputeHash(data);
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> SHA-1
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSHA1(string str)
        {
            var sha1 = new SHA1CryptoServiceProvider();
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha1.ComputeHash(data);
            sha1.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> SHA-256
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSHA256(string str)
        {
            var sha = new SHA256CryptoServiceProvider();
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> SHA-384
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSHA384(string str)
        {
            var sha = new SHA384CryptoServiceProvider();
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> SHA-512
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSHA512(string str)
        {
            var sha = new SHA512CryptoServiceProvider();
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> HmacMD5
        /// </summary>
        /// <param name="str"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string ToHmacMD5(string str, string password)
        {
            var passwordData = Encoding.UTF8.GetBytes(password);
            var sha = new HMACMD5(passwordData);
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> HmacSHA-1
        /// </summary>
        /// <param name="str"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string ToHmacSHA1(string str, string password)
        {
            var passwordData = Encoding.UTF8.GetBytes(password);
            var sha = new HMACSHA1(passwordData);
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> HmacSHA-256
        /// </summary>
        /// <param name="str"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string ToHmacSHA256(string str, string password)
        {
            var passwordData = Encoding.UTF8.GetBytes(password);
            var sha = new HMACSHA256(passwordData);
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> HmacSHA-384
        /// </summary>
        /// <param name="str"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string ToHmacSHA384(string str, string password)
        {
            var passwordData = Encoding.UTF8.GetBytes(password);
            var sha = new HMACSHA384(passwordData);
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// 字符串 -> HmacSHA-512
        /// </summary>
        /// <param name="str"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string ToHmacSHA512(string str, string password)
        {
            var passwordData = Encoding.UTF8.GetBytes(password);
            var sha = new HMACSHA512(passwordData);
            var data = Encoding.UTF8.GetBytes(str);
            var outBytes = sha.ComputeHash(data);
            sha.Dispose();
            return ToStr(outBytes);
        }

        /// <summary>
        /// Bytes -> string
        /// </summary>
        /// <param name="outBytes"></param>
        /// <returns></returns>
        private static string ToStr(byte[] outBytes)
        {
            var sb = new StringBuilder();
            foreach (var b in outBytes)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString().ToLower();
        }
    }
}
